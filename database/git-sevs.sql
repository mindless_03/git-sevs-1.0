-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 02, 2022 at 02:38 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `git-sevs`
--

-- --------------------------------------------------------

--
-- Table structure for table `election`
--

DROP TABLE IF EXISTS `election`;
CREATE TABLE IF NOT EXISTS `election` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `election`
--

INSERT INTO `election` (`id`, `status`, `start`, `end`, `name`) VALUES
(1, 3, '2022-03-25 05:02:06', '2022-03-25 05:03:08', 'pigsbugs@gmail.com'),
(2, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_05_29_045946_create_election_table', 1),
(9, '2018_05_29_051642_create_position_table', 1),
(10, '2018_05_29_052018_create_partylist_table', 1),
(11, '2018_05_29_100328_create_table_nominee', 1),
(12, '2018_05_29_100509_create_table_voter', 1),
(13, '2018_05_29_101506_create_table_result', 1),
(14, '2018_05_30_041251_alter_start_and_and_column_from_election_table', 1),
(15, '2018_05_31_031246_create_new_column_for_nominee_table', 1),
(16, '2018_06_01_154320_add_password_column_from_voter_table', 1),
(17, '2018_06_07_184552_add_columns_on_nominee_table', 1),
(18, '2021_07_10_224846_fix_nominee_image_column', 1),
(19, '2022_03_20_020014_create_online_user_tracker_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nominee`
--

DROP TABLE IF EXISTS `nominee`;
CREATE TABLE IF NOT EXISTS `nominee` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_id` int(10) UNSIGNED NOT NULL,
  `partylist_id` int(10) UNSIGNED DEFAULT NULL,
  `election_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'images/nominee/default.jpg',
  `motto` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nominee_election_id_foreign` (`election_id`),
  KEY `nominee_position_id_foreign` (`position_id`),
  KEY `nominee_partylist_id_foreign` (`partylist_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nominee`
--

INSERT INTO `nominee` (`id`, `created_at`, `updated_at`, `name`, `course`, `student_id`, `position_id`, `partylist_id`, `election_id`, `image`, `motto`, `description`) VALUES
(1, '2022-03-25 09:01:05', '2022-03-25 09:01:05', 'Gerald Anderson', 'BSIT', '5678', 1, 1, 1, 'images/nominee/default.jpg', 'kung san ka happy dun ka masaya', 'ge amping');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('7b8012dd6a5d9fdf065813988bf0db34dd4615c5064dd8b5cd31d50fd85d04748888f907d29916a5', 1, 3, 'My app', '[\"admin\"]', 0, '2022-03-25 06:47:37', '2022-03-25 06:47:37', '2023-03-25 02:47:37'),
('fc56cb9bad0ae3f5cb40322bed8ea76c1096e21f6a2c522ec742662960ef3410b6ce7868756d1ee1', 1, 3, 'My Token', '[\"vote\"]', 0, '2022-03-25 08:33:26', '2022-03-25 08:33:26', '2023-03-25 04:33:26'),
('89bc67295949005c727c8fc0581347cd55184e3cf4efe455e0febf1f4cdc44a104fdc64c6436bab9', 2, 3, 'My Token', '[\"vote\"]', 0, '2022-03-25 08:34:07', '2022-03-25 08:34:07', '2023-03-25 04:34:07'),
('2d24021ebd3d1f8346d9e83e5be70992b5173eb31000739bcde765a303eab46bfe47b5d725494569', 1, 3, 'My app', '[\"admin\"]', 0, '2022-03-25 08:48:52', '2022-03-25 08:48:52', '2023-03-25 04:48:52'),
('15cb2a0d71cbc766985419bb638d9abbc7dce8dc73708769c5c2388bf78d07c52bdafe11ee1c46ac', 1, 3, 'My Token', '[\"vote\"]', 0, '2022-03-25 09:02:28', '2022-03-25 09:02:28', '2023-03-25 05:02:28'),
('16e6b9777774a55d6e79ff60d352ee397446bccb12a47462c347f927b2abd3c5e6a4c874ccf25fc3', 1, 3, 'My app', '[\"admin\"]', 0, '2022-03-25 09:02:56', '2022-03-25 09:02:56', '2023-03-25 05:02:56');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'FjOGbS8zFp75au3qTCXlM2yRfDPNLEQvlln2I4aK', 'http://localhost', 1, 0, 0, '2022-03-25 06:44:50', '2022-03-25 06:44:50'),
(2, NULL, 'Laravel Password Grant Client', 'IwRyp31lJfdPGdEvCeXaGYbRIfRN9Q45d1zv2qOo', 'http://localhost', 0, 1, 0, '2022-03-25 06:44:50', '2022-03-25 06:44:50'),
(3, NULL, 'Laravel Personal Access Client', 'HuxvvQNTv3s9yP2ymO8FdnJQo38biB3PR48n4Ww6', 'http://localhost', 1, 0, 0, '2022-03-25 06:45:10', '2022-03-25 06:45:10'),
(4, NULL, 'Laravel Password Grant Client', 'TD6FK9bxa448Hk5w3IAYm2FGfU3FcIN3hASWdKU3', 'http://localhost', 0, 1, 0, '2022-03-25 06:45:10', '2022-03-25 06:45:10');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2022-03-25 06:44:50', '2022-03-25 06:44:50'),
(2, 3, '2022-03-25 06:45:10', '2022-03-25 06:45:10');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `online_user_tracker`
--

DROP TABLE IF EXISTS `online_user_tracker`;
CREATE TABLE IF NOT EXISTS `online_user_tracker` (
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `online_user_tracker_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partylist`
--

DROP TABLE IF EXISTS `partylist`;
CREATE TABLE IF NOT EXISTS `partylist` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `election_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `partylist_election_id_foreign` (`election_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partylist`
--

INSERT INTO `partylist` (`id`, `name`, `created_at`, `updated_at`, `election_id`) VALUES
(1, 'Low priority partylist', '2022-03-25 09:00:32', '2022-03-25 09:00:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
CREATE TABLE IF NOT EXISTS `position` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `election_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `position_election_id_foreign` (`election_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `name`, `created_at`, `updated_at`, `election_id`) VALUES
(1, 'President', '2022-03-25 08:49:18', '2022-03-25 08:49:18', 1),
(2, 'VP', '2022-03-25 08:49:25', '2022-03-25 08:49:25', 1),
(3, 'Secretary', '2022-03-25 08:49:37', '2022-03-25 08:49:37', 1),
(4, 'Treasurer', '2022-03-25 08:49:51', '2022-03-25 08:49:51', 1),
(5, 'President', '2022-03-26 01:54:28', '2022-03-27 04:55:52', 2),
(6, 'Secretary', '2022-03-26 01:55:06', '2022-03-26 01:55:06', 2),
(7, 'Treasurer', '2022-04-03 02:35:59', '2022-04-03 02:35:59', 2),
(8, 'PRO', '2022-04-03 02:36:18', '2022-04-03 02:36:18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
CREATE TABLE IF NOT EXISTS `result` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voter_id` int(10) UNSIGNED NOT NULL,
  `position_id` int(10) UNSIGNED NOT NULL,
  `nominee_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `election_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `result_election_id_foreign` (`election_id`),
  KEY `result_position_id_foreign` (`position_id`),
  KEY `result_voter_id_foreign` (`voter_id`),
  KEY `result_nominee_id_foreign` (`nominee_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Paolo Jem Belocora', 'pigsbugs@gmail.com', '$2y$10$kGS5vbjmq5FRb9wuHgrjNugJ03eByJq1PiFkn6CF2IxeMqeBKgcrm', NULL, '2022-04-02 09:01:46', '2022-04-02 09:01:46');

-- --------------------------------------------------------

--
-- Table structure for table `voter`
--

DROP TABLE IF EXISTS `voter`;
CREATE TABLE IF NOT EXISTS `voter` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `election_id` int(10) UNSIGNED NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '$2y$10$hXmqiO04.yqsQ5xi0hEGqOFSrZvF/oWucNquf.L8LNuNDozj67jl2',
  PRIMARY KEY (`id`),
  KEY `voter_election_id_foreign` (`election_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voter`
--

INSERT INTO `voter` (`id`, `name`, `student_id`, `course`, `created_at`, `updated_at`, `election_id`, `password`) VALUES
(1, 'Edmar Lumacad', '12345', 'BSIT', '2022-03-25 08:32:19', '2022-03-25 08:32:19', 1, '$2y$10$hXmqiO04.yqsQ5xi0hEGqOFSrZvF/oWucNquf.L8LNuNDozj67jl2'),
(2, 'John Mike Degracia', '54321', 'BSIT', '2022-03-25 08:32:52', '2022-03-25 08:32:52', 1, '$2y$10$hXmqiO04.yqsQ5xi0hEGqOFSrZvF/oWucNquf.L8LNuNDozj67jl2');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
